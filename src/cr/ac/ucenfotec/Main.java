package cr.ac.ucenfotec;

import cr.ac.ucenfotec.tl.PersonaController;

public class Main {

    public static void main(String[] args) throws Exception{
        PersonaController controlador = new PersonaController();
        controlador.start();
    }

}
