package cr.ac.ucenfotec.bl.entities.persona;

import java.util.ArrayList;

public interface IPersonaDAO {
    String registarPersona(Persona persona) throws Exception;
    String actualizarPersona(Persona persona) throws Exception;
    String eliminarPersona(int cedula) throws Exception;
    ArrayList<Persona> listarPersona() throws Exception;
}
