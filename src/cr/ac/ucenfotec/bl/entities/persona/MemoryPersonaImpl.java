package cr.ac.ucenfotec.bl.entities.persona;

import java.util.ArrayList;

public class MemoryPersonaImpl implements IPersonaDAO{

    public String registarPersona(Persona persona) throws Exception {
        return "Se registro la persona en Memoria";
    }

    public String actualizarPersona(Persona persona) throws Exception {
        return "Se actualizó la persona en Memoria";
    }

    public String eliminarPersona(int cedula) throws Exception {
        return "Se eliminó la persona en memoria";
    }

    public ArrayList<Persona> listarPersona() throws Exception {
        return new ArrayList<>();
    }
}
