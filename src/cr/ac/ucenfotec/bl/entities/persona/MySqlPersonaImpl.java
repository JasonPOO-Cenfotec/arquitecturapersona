package cr.ac.ucenfotec.bl.entities.persona;

import cr.ac.ucenfotec.dl.bd.Conector;

import java.sql.ResultSet;
import java.util.ArrayList;

public class MySqlPersonaImpl implements IPersonaDAO{

    private String sql;

    public String registarPersona(Persona persona) throws Exception {
        sql = "INSERT INTO PERSONA VALUES ("+persona.getCedula()+",'"+persona.getNombre()+"')";
        Conector.getConnector().ejecutarSQL(sql);

        return "Se registro la persona en MySQL";
    }

    public String actualizarPersona(Persona persona) throws Exception {
        sql = "UPDATE PERSONA SET NOMBRE='"+persona.getNombre()+"' WHERE CEDULA = "+persona.getCedula()+"";
        Conector.getConnector().ejecutarSQL(sql);

        return "Se actualizó la persona en MySQL";
    }

    public String eliminarPersona(int cedula) throws Exception {
        sql = "DELETE FROM PERSONA WHERE CEDULA="+cedula+"";
        Conector.getConnector().ejecutarSQL(sql);

        return "Se eliminó la persona en MySQL";
    }

    public ArrayList<Persona> listarPersona() throws Exception {
        ArrayList<Persona> listaPersonas = new ArrayList<>();

        sql="SELECT CEDULA, NOMBRE FROM PERSONA ORDER BY CEDULA ASC";
        ResultSet rs = Conector.getConnector().ejecutarQuery(sql);

        while(rs.next()){//Ciclo para crear los objetos y llenar la lista
            Persona persona = new Persona(rs.getInt("CEDULA"),rs.getString("NOMBRE"));
            listaPersonas.add(persona);
        }
        return listaPersonas;
    }
}
