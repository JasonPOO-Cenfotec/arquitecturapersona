package cr.ac.ucenfotec.bl.logic;

import cr.ac.ucenfotec.bl.entities.persona.IPersonaDAO;
import cr.ac.ucenfotec.bl.entities.persona.MemoryPersonaImpl;
import cr.ac.ucenfotec.bl.entities.persona.MySqlPersonaImpl;
import cr.ac.ucenfotec.bl.entities.persona.Persona;

import java.util.ArrayList;

public class PersonaGestor {

    private IPersonaDAO personaDAO;

    public PersonaGestor(){
        personaDAO = new MySqlPersonaImpl();
    }

    public String registarPersona(int cedula, String nombre) throws Exception{
        Persona persona = new Persona(cedula,nombre);
        return personaDAO.registarPersona(persona);
    }

    public String actualizarPersona(int cedula, String nombre) throws Exception{
        Persona persona = new Persona(cedula,nombre);
        return personaDAO.actualizarPersona(persona);
    }

    public String eliminarPersona(int cedula) throws Exception{
        return personaDAO.eliminarPersona(cedula);
    }

    public ArrayList<Persona> listarPersonas() throws Exception{
        return personaDAO.listarPersona();
    }
}
