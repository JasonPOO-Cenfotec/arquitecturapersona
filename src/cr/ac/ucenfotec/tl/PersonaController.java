package cr.ac.ucenfotec.tl;

import cr.ac.ucenfotec.bl.entities.persona.Persona;
import cr.ac.ucenfotec.bl.logic.PersonaGestor;
import cr.ac.ucenfotec.ui.UI;

public class PersonaController {

    private UI interfaz;
    private PersonaGestor gestor;

    public PersonaController(){
        interfaz = new UI();
        gestor = new PersonaGestor();
    }

    public void start() throws Exception{
        int opcion = -1;
        do{
            interfaz.mostrarMenu();
            opcion = interfaz.leerNumero();
            procesarOpcion(opcion);
        }while(opcion!=0);
    }

    public void procesarOpcion(int opcion) throws Exception{
        switch (opcion){
            case 1: registrarPersona();
                break;
            case 2: actualizarPersona();
                break;
            case 3: eliminarPersona();
                break;
            case 4: listarPersonas();
                break;
            case 0: interfaz.imprimirMensaje("Gracias por su visita!");
            break;
            default:
                interfaz.imprimirMensaje("Opción invalida");
                break;
        }
    }

    public void registrarPersona() throws Exception {
        interfaz.imprimirMensaje("Por favor digite la cedula: ");
        int cedula = interfaz.leerNumero();
        interfaz.imprimirMensaje("Por favor digite el nombre: ");
        String nombre = interfaz.leerTexto();

        String mensaje = gestor.registarPersona(cedula,nombre);
        interfaz.imprimirMensaje(mensaje);
    }

    public void actualizarPersona() throws Exception{
        interfaz.imprimirMensaje("Por favor digite la cedula: ");
        int cedula = interfaz.leerNumero();
        interfaz.imprimirMensaje("Por favor digite el nombre: ");
        String nombre = interfaz.leerTexto();

        String mensaje  = gestor.actualizarPersona(cedula,nombre);
        interfaz.imprimirMensaje(mensaje);
    }

    public void eliminarPersona() throws Exception{
        interfaz.imprimirMensaje("Por favor digite la cedula: ");
        int cedula = interfaz.leerNumero();

        String mensaje= gestor.eliminarPersona(cedula);
        interfaz.imprimirMensaje(mensaje);
    }

    public void listarPersonas() throws Exception{
        for (Persona persona:gestor.listarPersonas()) {
            interfaz.imprimirMensaje(persona.toString());
        }
    }
}
