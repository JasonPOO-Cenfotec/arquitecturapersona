package cr.ac.ucenfotec.ui;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class UI {
    private BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

    public void mostrarMenu(){
        System.out.println("***** Bienvenido al Sistema *****");
        System.out.println("1. Registrar Persona.");
        System.out.println("2. Actualizar Persona.");
        System.out.println("3. Eliminar Persona.");
        System.out.println("4. Listar Persona.");
        System.out.println("0. Salir.");
        System.out.print("Por favor ingrese la opción que desea: ");
    }

    public int leerNumero() throws Exception{
       return Integer.parseInt(in.readLine());
    }

    public void imprimirMensaje(String mensaje){
        System.out.println(mensaje);
    }

    public String leerTexto() throws Exception{
         return in.readLine();
    }
}
